from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .forms import UserRegistrationForm
from django.contrib.auth.models import User
from .forms import UserEditForm, ProfileEditForm
from django.contrib.auth import login
from .models import Profile
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.shortcuts import render, redirect, reverse

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    # Authentication successful
                    login(request, user)
                    return HttpResponse("Authenticated successfully")
                else:
                    # Inactive user
                    return HttpResponse("Disabled account")
            else:
                # Invalid login
                return HttpResponse("Invalid login")
    else:
        form = LoginForm()

    return render(request, 'account/login.html', {'form': form})


@login_required
def dashboard(request):
    return render(request, 'account/dashboard.html')


User = get_user_model()

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            email = user_form.cleaned_data['email']

            # Check if the email is already in use
            if User.objects.filter(email=email).exists():
                user_form.add_error('email', 'This email is already in use.')
            else:
                new_user = user_form.save(commit=False)
                new_user.set_password(user_form.cleaned_data['password'])
                new_user.save()
                Profile.objects.create(user=new_user)
                return redirect(reverse('account:login'))
    else:
        user_form = UserRegistrationForm()

    return render(request, 'account/register.html', {'user_form': user_form})

def register_done(request, username):
    new_user = User.objects.get(username=username)
    return render(request, 'account/register_done.html', {'new_user': new_user})

@login_required
def edit_profile(request):
    user = request.user
    

    if request.method == 'POST':
        user_form = UserEditForm(instance=user, data=request.POST)
        profile_form = ProfileEditForm(instance=user.profile, data=request.POST, files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
    else:
        user_form = UserEditForm(instance=user)
        profile_form = ProfileEditForm(instance=user.profile)

    return render(request, 'account/edit.html', {'user_form': user_form, 'profile_form': profile_form})

from django.contrib import messages
from django.shortcuts import render, redirect

def edit_view(request):
    
    messages.success(request, 'Changes saved successfully.')
    messages.error(request, 'Something went wrong while saving changes.')

    return redirect(reverse('account:login'))