from django.urls import path, include
from . import views
from django.contrib.auth.views import LogoutView
app_name = 'account'

urlpatterns = [
    path('login/', views.user_login, name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('register/', views.register, name='register'),
    path('register_done/<str:username>/', views.register_done, name='register_done'),
    path('edit/', views.edit_profile, name='edit_profile'),
    path('', include('django.contrib.auth.urls')),
]